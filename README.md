# notes

# Notes App

- a user can register for an account
- a user can reset their password via an email link
- a user can log in to their account
- notes are private to a user account
- a user can create new notes